export const state = () => ({
    inputAnimation: {
        reversed: false
    },

    listAnimation: {
        el: null
    }
})

export const getters = {
    getAnimation( state ) {
        return state.inputAnimation.reversed
    }
}

export const mutations = {
    addAnimation( state , { animation, params }) {
        if( params == "inputAnimation" ) {
           state.inputAnimation.reversed = animation
        }

        if( params == "listAnimation" ) {
            state.listAnimation.el = animation
            
        }
    }
}

export const actions = {

}



