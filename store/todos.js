export const state = () => ({
    todos: [
        {   desc: "makan",
            favorited: true,
            done: false
        },
        {   desc: "tidur",
            favorited: false,
            done: true
        }
    ]
})

export const mutations = {
    addTodo(state, payload) {
        state.todos.unshift(payload)
        return console.log(state.todos)
    }
}

export const getters = {
    favorited: state => state.todos.filter( todo => todo.favorited ),
    done: state => state.todos.filter( todo => todo.done ),
    important: state => state.todos.filter( todo => todo.important )
}

export const actions = {
    asyncAddTodo({commit, state}, payload) {

        let data = {
            desc: payload,
            favorited: false,
            done: false
        }

        try {
            commit('addTodo', data)
            console.log("todo added") 
            
        } catch (error) {
            console.log(error)    
        }
    }
}