import { TimelineMax, Expo, TweenMax } from 'gsap'

export const ListAnimationMixin = {
    data() {
        return {
            listAnimation: null,
            newTodo: null,
            destination: 0,
            tempDestinationVal: 0,
            animating: false,
            currentOffset: 0
        }
    },
    computed: {
        el() {
           return this.$store.state.animation.listAnimation.el
        },
        
        parentEl() {
            return this.el.parentElement
        }
    },

    methods: {
        async initListAnimation( el ) {
            // console.log(this.$store.state.todos.todos , 'mixin')
            // console.log(getComputedStyle(el.children[0]).cssText)
            // console.log(getComputedStyle(el).cssText)
            //Define Element and Copy element
            
            // let newTodo = document.createElement('li')
            // newTodo.innerHTML = "halo"
            // newTodo.style.cssText = getComputedStyle(el.children[0]).cssText
            // newTodo.style.opacity = 0
            // newTodo.style.left = '51px'
            
            // Object.entries(el.children).map( (key, val) => {
            //     console.log(key, val)
            //     
            // } )

            this.$store.commit('animation/addAnimation',{ animation: el, params: "listAnimation"})
            el.style.position = "relative"
            console.log(this.parentEl)
            // console.log(el.children[3] )            

            // el.prepend(newTodo)
            // let promise = new Promise( (resolve, reject) => {
            //     TweenMax.to( newTodo, 2, { left: 0 ,opacity: 1 , ease: Expo.easeOut, delay: 1 } )
            //         setTimeout( () => { 
            //             resolve("foo") 
            //             // el.removeChild(newTodo)
            //         }, 3000 )
            // })
            // promise
            // .then(res => { this.$store.dispatch("todos/asyncAddTodo", "haloo") , console.log("berhasil")} )
            
            
        },

        addTodo( textInput ) {
            if(this.animating) 
            return
            this.animating = true
            // this.newTodo ? this.newTodo.style = '' : false;
            let newTodo = document.createElement('li')
            newTodo.innerHTML = textInput
            newTodo.style.cssText = getComputedStyle(this.el.children[0]).cssText
            newTodo.style.opacity = 1
            // newTodo.style.left = '51px'
            newTodo.style.position = 'absolute'
            this.newTodo = newTodo
            
            let childHeight = this.el.children[0].offsetHeight
            let marginBottom = parseFloat(
                getComputedStyle(this.el.children[0])
                    .getPropertyValue("margin-bottom")
                    .split("px")[0]
            );
            this.el.parentNode.insertBefore(newTodo, this.el)
            this.offset = (childHeight + marginBottom)
           
            // let newTodo = this.el.children[0]
            
            // this.$nextTick( ()=> {
                
            
                // this.el.removeChild(newTodo)
                // let animPromise = new Promise ( (resolve, reject) => { 
                    let elementCount = 0
                    console.log(this.listAnimation, "animation")
                    this.listAnimation = this.listAnimation ? this.listAnimation : new TimelineMax()
                    // this.el.children.forEach( ( element ) => {
                    //     console.log(element)
                    //     let elementPosition = this.offset * elementCount
                    //     element.style.position  =  'absolute'
                    //     // element.style.top  =  `${elementPosition}px`
                            
                    //     ++elementCount
                        
                    //     this.animation.fromTo( element, .4, { top: elementPosition  }, { top: this.offset * elementCount , ease: Expo.easeOut  } )
                        
                    //     return 
                    // });

                    
                    this.listAnimation.to( this.el, .4 , { y: this.offset, ease: Expo.easeOut, onComplete: ()=>this.doneAdd(textInput)  })
                    
                    // this.el.parentNode.removeChild(newTodo)
                    // this.el.append(newTodo)
                    
                    setTimeout(() => {
                    //     // this.el.style = ''
                        // this.listAnimation.set( this.el, .4 , { y: 0})
                        // this.$store.dispatch("todos/asyncAddTodo", textInput)
                    }, 1000);
                    
                    
                // })
            // })

            // let ans = new TimelineMax()
            // ans.to( this.el, .4, { y: this.offset, ease: Expo.easeOut } )
            // setTimeout(() => {
            //     ans.reverse()
            // }, 1000);
                // animPromise.then( () => { 
                    
                //         this.el.prepend(newTodo)
                   
                // })
                // .then( ()=> {
                //     newTodo.style.top = 0
                //     newTodo.style.opacity = 0
                //     newTodo.style.position = "absolute"
                //     TweenMax.to(newTodo, .4, { opacity: 1, ease: Expo.easeOut })
                // })
    
            

            
            
            
            console.log(this.el.children)
        },

        doneAdd(textInput) {
            console.log(this.listAnimation)
            // this.listAnimation.restart()
            this.el.parentNode.removeChild(this.newTodo)
            this.$store.dispatch("todos/asyncAddTodo", textInput)
            this.el.style = ''
            this.listAnimation.set( this.el, { y: 0})
            this.animating = false
            
        },

        initListScrollAnimation( params ) {
            this.currentOffset = !this.currentOffset ? this.currentOffset = params : this.currentOffset
            let parentBound = window.innerWidth / 100 * 5
            console.log( parentBound)
            this.destination = this.el.offsetHeight + this.el.offsetTop + parentBound
            this.moveField = window.innerHeight - this.el.offsetTop
            let boundaries = this.el.offsetHeight / 2
            let offset = params == 0 ? 1 : params
            let direction =  this.moveField / this.el.offsetHeight
            console.log(direction)

            if( params > this.currentOffset && this.destination > window.innerHeight ) {
                TweenMax.to( this.el, 1 , { y: -direction, ease: Power0.easeNone } )
            }

            if( params < this.currentOffset && this.destination > window.innerHeight ) {
                TweenMax.to( this.el, 1 , { y: direction, ease: Power0.easeNone } )
            }
            this.currentOffset = params
            console.log(this.currentOffset, this.el.offsetTop, window.innerHeight )
        }

    },

    mounted() {

    }
}